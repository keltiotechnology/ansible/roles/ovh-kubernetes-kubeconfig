# Ovh kubernetes kubeconfig

Get kubeconfig for a specific kubernetes cluster on OVH.

## Local prerequisites
- pip packages: ovh + requests (see requirements.txt)

## Vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| ovh_kubernetes_cluster_name | The cluster name of the cluster to get kubeconfig | `string` | `` | yes |
| ovh_kubernetes_serviceName | The ovh kubernetes project name | `string` | `` | yes |
| secret_ovh_application_key | The ovh application key | `string` | `` | yes |
| secret_ovh_application_secret | The ovh application secret | `string` | `` | yes |
| secret_ovh_consumer_key | The ovh consumer key | `string` | `` | yes |
| ovh_endpoint | The ovh endpoint | `string` | `ovh-eu` | no |
| ovh_kubernetes_cluster_path_kubeconfig | Path to cluster kubeconfig | `string` | `` | yes |
