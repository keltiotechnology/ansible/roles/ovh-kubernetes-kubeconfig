# -*- encoding: utf-8 -*-
'''
First, install the latest release of Python wrapper: $ pip install ovh
# /cloud/project/%7BserviceName%7D/kube#POST
Then watch the api doc here: https://api.ovh.com/console/
'''
import os
import hashlib
import json
import ovh
import time
import requests



########################################################################
#
# OVH CONF
#
########################################################################
ovh_endpoint = os.environ['OVH_ENDPOINT']
ovh_application_key = os.environ['OVH_APPLICATION_KEY']
ovh_application_secret = os.environ['OVH_APPLICATION_SECRET']
ovh_consumer_key = os.environ['OVH_CONSUMER_KEY']
serviceName = os.environ['OVH_SERVICE_NAME']

ENDPOINTS = {
    'ovh-eu': 'https://eu.api.ovh.com/1.0',
    'ovh-us': 'https://api.us.ovhcloud.com/1.0',
    'ovh-ca': 'https://ca.api.ovh.com/1.0',
    'kimsufi-eu': 'https://eu.api.kimsufi.com/1.0',
    'kimsufi-ca': 'https://ca.api.kimsufi.com/1.0',
    'soyoustart-eu': 'https://eu.api.soyoustart.com/1.0',
    'soyoustart-ca': 'https://ca.api.soyoustart.com/1.0',
}


########################################################################
#
# CLUSTER CONF
#
########################################################################
cluster_name = os.environ['CLUSTER_NAME']
path_to_save_kube_config = os.environ['CLUSTER_KUBECONFIG_PATH']

client = ovh.Client(
    endpoint=ovh_endpoint,
    application_key=ovh_application_key,
    application_secret=ovh_application_secret,
    consumer_key=ovh_consumer_key
)


########################################################################
#
# METHODS
#
########################################################################
def get_kubernetes_clusters_ids():
    result = client.get(
        '/cloud/project/{serviceName}/kube'.format(serviceName=serviceName)
    )
    return result


def get_kubernetes_cluster_data(cluster_id):
    result = client.get(
        '/cloud/project/{serviceName}/kube/{cluster_id}'.format(
            serviceName=serviceName, cluster_id=cluster_id)
    )
    return result


def get_kubernetes_cluster_kubeconfig(cluster_name):
    if os.path.isfile(path_to_save_kube_config):
        print("Kubeconfig already exist")
        return

    clusters_ids = get_kubernetes_clusters_ids()
    print('Existing clusters')
    print(json.dumps(clusters_ids, indent=4))
    cluster_id = None
    for clu_id in clusters_ids:
        cluster_data = get_kubernetes_cluster_data(clu_id)
        if cluster_data['name'] == cluster_name:
            cluster_id = clu_id
            break

    if not cluster_id:
        raise Exception('Cluster {} not found'.format(cluster_name))

    # CANNOT USE THE CLIENT FOR THIS METHOD
    # Issue: https://github.com/ovh/python-ovh/pull/85
    # Workflow réalisé à partir du code source: https://github.com/ovh/python-ovh/blob/master/ovh/client.py
    target = '{ovh_endpoint_url}/cloud/project/{serviceName}/kube/{cluster_id}/kubeconfig'.format(
        ovh_endpoint_url=ENDPOINTS[ovh_endpoint],
        serviceName=serviceName,
        cluster_id=cluster_id
    )
    body = ''
    now = str(int(time.time()))
    signature = hashlib.sha1()
    signature.update("+".join([
        ovh_application_secret,
        ovh_consumer_key,
        'POST',
        target,
        body,
        now
    ]).encode('utf-8'))
    headers = {
        'X-Ovh-Application': ovh_application_key,
        'X-Ovh-Consumer': ovh_consumer_key,
        'X-Ovh-Timestamp': now,
        'X-Ovh-Signature': "$1$" + signature.hexdigest()
    }

    result = requests.post(target, headers=headers)

    with open(path_to_save_kube_config, 'w') as f:
        f.write(result.json()['content'])
    print('Kubeconfig created')
    return


get_kubernetes_cluster_kubeconfig(cluster_name)
